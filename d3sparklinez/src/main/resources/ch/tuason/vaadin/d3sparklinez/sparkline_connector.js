/**
 * 28.10.2014 / designed by marcel s.
 */
"use strict";

window.ch_tuason_vaadin_d3sparklinez_Sparkline = function() {

    var config = {
        width: this.getState().sparkWidth,
        height: this.getState().sparkHeight,
        name: this.getState().name,
        value: this.getState().value,
        values: this.getState().values,
        numberOfPoints: this.getState().numberOfPoints,
        sparkColor: this.getState().config.sparkColor,
        showEndPoint: this.getState().config.showEndPoint,
        colorEndPoint: this.getState().config.endpointColor
    };

    var currentSparkline = new Sparkline(config, this.getElement());
    currentSparkline.render();

    // TODO this needs to be activated if we extend the component with redraws...
    /*
    this.onStateChange = function() {
        currentSparkline.redraw(this.getState().values, 2000);
    };
    */
};



function Sparkline(configuration, sparklineElement) {

    var numberOfPoints = null;
    var raw_data = null;
    var data = [];
    var value = null;
    var width = null;
    var height = null;
    var x = null;
    var y = null;
    var showEndPoint = false;
    var strokeColor = null;
    var endpointColor = null;
    var line = null;

    var self = this; // this is needed for internal d3 functions...



    this.configure = function(configuration) {
        this.config = configuration;

        width = this.config.width;
        height = this.config.height;

        numberOfPoints = this.config.numberOfPoints;
        raw_data = this.config.values;
        value = this.config.value;

        if (this.config.sparkColor)
            strokeColor = this.config.sparkColor;

        showEndPoint = this.config.showEndPoint;
        endpointColor = this.config.colorEndPoint;

        x = d3.scale.linear().range([0, width - 2]);
        y = d3.scale.linear().range([height - 4, 0]);
        line = d3.svg.line()
                .interpolate("basis")
                .x(function(d) {
                    return x(d.x_date);
                })
                .y(function(d) {
                    return y(d.y_value);
                });
    };


    this.render = function() {
        var i=0;
        raw_data.forEach(function(d) {
            var elementInData = {
                x_date: i,
                y_value: d
            };
            data.push(elementInData);
            i++;
        });

        x.domain(d3.extent(data, function(d) {
            return d.x_date; })
        );
        y.domain(d3.extent(data, function(d) {
            return d.y_value;
        }));

        var svg = d3.select(sparklineElement)
                .append('svg:svg')
                .attr('width', width)
                .attr('height', height);


        svg.append('svg:path')
                // .append('g')
                .attr('class', 'sparkline')
                .attr('transform', 'translate(0, 2)')
                .attr('d', line(data));

        if (strokeColor) {
            svg.append('svg:path')
                // .append('g')
                .attr('class', 'sparkline')
                .style( 'stroke', strokeColor )
                .attr('transform', 'translate(0, 2)')
                .attr('d', line(data));
        } else {
            svg.append('svg:path')
                // .append('g')
                .attr('class', 'sparkline')
                .attr('transform', 'translate(0, 2)')
                .attr('d', line(data));
        }


        if (showEndPoint == true) {
            svg.append('circle')
                 .attr('cx', x(data[data.length - 1].x_date))
                 .attr('cy', y(data[data.length - 1].y_value))
                 .attr('r', 1.5 )
                 .style('fill', endpointColor);
        }

        this.body = svg;
    };


    this.redraw = function(values, transitionDuration)
    {
        // TODO -  the redrawing method!
    };


    // initialization
    this.configure(configuration);
}



