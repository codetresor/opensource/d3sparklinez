package ch.tuason.vaadin.d3sparklinez;

import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.AbstractJavaScriptComponent;

/**
 * Created by maesi on 28.10.14.
 */
@JavaScript({"d3.min.js", "sparkline_connector.js"})
@StyleSheet({"sl-styles.css"})
public class Sparkline extends AbstractJavaScriptComponent
{

    public static final int STD_WIDTH = 100;
    public static final int STD_HEIGHT = 25;


    /**
     * constructor 1
     * @param name
     * @param values
     */
    public Sparkline(String name, int[] values) {
        this( name, STD_WIDTH, STD_HEIGHT, values, 0, 0 );
    }


    /**
     * constructor 2
     * @param name
     * @param values
     * @param config
     */
    public Sparkline(String name, int[] values, SparklineConfig config) {
        this( name, STD_WIDTH, STD_HEIGHT, values, 0, 0, config );
    }

    /**
     * constructor 3
     * @param name
     * @param width
     * @param height
     */
    public Sparkline(String name, Integer width, Integer height, int[] values, int numberOfPoints, int value) {
        this( name, width, height, values, numberOfPoints, value, new SparklineConfig() );
    }

    /**
     * constructor 4
     * @param name
     * @param width
     * @param height
     * @param values
     * @param numberOfPoints
     * @param value
     * @param config
     */
    public Sparkline(String name, Integer width, Integer height, int[] values, int numberOfPoints, int value, SparklineConfig config) {
        getState().setName( name );
        getState().setSparkWidth( width );
        getState().setSparkHeight( height );
        getState().setValues( values );
        if (values.length > 0 && numberOfPoints <= 0)
            getState().setNumberOfPoints( values.length );
        else
            getState().setNumberOfPoints( numberOfPoints );

        if (values.length > 0 && value <= 0)
            getState().setValue( values[values.length - 1] );
        else
            getState().setValue( value );
        getState().setConfig( config );
    }

    @Override
    protected SparklineState getState() {
        return (SparklineState) super.getState();
    }

    public void setValues(int[] values) {
        getState().setValues( values );
    }

    public int[] getValues() {
        return getState().getValues();
    }

    public void setNumberOfPoints(int numberOfPoints) {
        getState().setNumberOfPoints( numberOfPoints );
    }

    public int getNumberOfPoints() {
        return getState().getNumberOfPoints();
    }
}
