package ch.tuason.vaadin.d3sparklinez;

import com.vaadin.shared.ui.colorpicker.Color;

import java.io.Serializable;

/**
 * Created by maesi on 28.10.14.
 */
public class SparklineConfig implements Serializable
{


    private String sparkColor;
    private String endpointColor;
    private boolean showEndPoint;


    /**
     * constructor
     */
    public SparklineConfig() {
        this.sparkColor = null; //Color.BLACK.getCSS();
        this.endpointColor = Color.RED.getCSS();
        this.showEndPoint = false;
    }

    /**
     * constructor 2
     *
     * @param sparkColor
     * @param endpointColor
     * @param showEndpoint
     */
    public SparklineConfig(String sparkColor, String endpointColor, boolean showEndpoint) {
        if (sparkColor != null && !sparkColor.trim().equals( "" ))
            this.sparkColor = sparkColor;
        else
            this.sparkColor = null;

        if (endpointColor != null && !endpointColor.trim().equals( "" ))
            this.endpointColor = endpointColor;
        else
            this.endpointColor = Color.RED.getCSS();


        this.showEndPoint = showEndpoint;
    }

    public String getSparkColor()
    {
        return sparkColor;
    }

    public void setSparkColor( String sparkColor )
    {
        this.sparkColor = sparkColor;
    }

    public boolean isShowEndPoint()
    {
        return showEndPoint;
    }

    public void setShowEndPoint( boolean showEndPoint )
    {
        this.showEndPoint = showEndPoint;
    }

    public String getEndpointColor()
    {
        return endpointColor;
    }

    public void setEndpointColor( String endpointColor )
    {
        this.endpointColor = endpointColor;
    }
}

