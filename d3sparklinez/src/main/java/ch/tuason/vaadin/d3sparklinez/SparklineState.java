package ch.tuason.vaadin.d3sparklinez;

import com.vaadin.shared.ui.JavaScriptComponentState;

/**
 * Created by maesi on 28.10.14.
 */
public class SparklineState extends JavaScriptComponentState
{
    private int numberOfPoints;
    private int[] values;

    private Integer sparkWidth;
    private Integer sparkHeight;
    private Integer value;
    private String name;
    private SparklineConfig config;

    public Integer getSparkWidth() {
        return sparkWidth;
    }

    public void setSparkWidth(Integer size) {
        this.sparkWidth = size;
    }

    public Integer getSparkHeight() {
        return sparkHeight;
    }

    public void setSparkHeight(Integer size) {
        this.sparkHeight = size;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SparklineConfig getConfig() {
        return config;
    }

    public void setConfig(SparklineConfig config) {
        this.config = config;
    }

    public int getNumberOfPoints()
    {
        return numberOfPoints;
    }

    public void setNumberOfPoints( int numberOfPoints )
    {
        this.numberOfPoints = numberOfPoints;
    }

    public int[] getValues()
    {
        return values;
    }

    public void setValues( int[] values )
    {
        this.values = values;
    }


}
