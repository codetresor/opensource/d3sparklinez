package ch.tuason.vaadin.d3sparklinez.demo;

import ch.tuason.vaadin.d3sparklinez.Sparkline;
import ch.tuason.vaadin.d3sparklinez.SparklineConfig;
import com.github.wolfie.refresher.Refresher;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import javax.servlet.annotation.WebServlet;

@Theme("demo")
@Title("...::: D3.js Sparklinez Component - Demo App :::...")
@SuppressWarnings("serial")
public class SparklinezDemoUI
    extends UI
{

    @WebServlet(value = "/*",
                asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false,
                                ui = SparklinezDemoUI.class,
                                widgetset = "ch.tuason.vaadin.d3sparklinez.demo.DemoWidgetSet")
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {

        // Initialize our new UI component
        int[] valuesForSparklines = getRandomSparklineValues( 50, 1, 100 );
        Sparkline sparkline_one = new Sparkline("test", valuesForSparklines);

        Sparkline sparkline_two = new Sparkline( "sparkline_two",
                                                 getRandomSparklineValues( 80, 0, 100 ),
                                                 new SparklineConfig( Color.BLUE.getCSS(),
                                                                      Color.YELLOW.getCSS(),
                                                                      true));

        Sparkline sparkline_three = new Sparkline( "sparkline_three",
                                                 getRandomSparklineValues( 50, 0, 10 ),
                                                 new SparklineConfig( Color.GREEN.getCSS(),
                                                                      Color.RED.getCSS(),
                                                                      true));

        // Show it in the middle of the screen
        final VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setStyleName("demoContentLayout");
        layout.setMargin( true );
        layout.setSpacing( true );
        layout.addComponent(new Label(".::: A FEW SPARKLINES TO TEST :::..."));
        layout.addComponent(sparkline_one);
        layout.addComponent( sparkline_two );
        layout.addComponent( sparkline_three );
        // layout.setComponentAlignment(component, Alignment.MIDDLE_CENTER);

        setContent( layout );

        startUIUpdate();
    }


    private void updateSparklines() {
        System.out.println( "sparklinez update..." );
    }


    /**
     * starting the ui refresher task...
     */
    private void startUIUpdate()
    {
        Refresher refresher = new Refresher();
        refresher.setRefreshInterval(5000);
        refresher.addListener(new Refresher.RefreshListener() {
            @Override
            public void refresh(Refresher refresher) {
                updateSparklines();
            }
        });

        addExtension( refresher );
    }


    /**
     * returns an array with random integers...
     *
     * @param howMany
     * @param min
     * @param max
     * @return
     */
    private int[] getRandomSparklineValues(int howMany, int min, int max) {
        int[] values = new int[howMany];

        for (int i = 0; i < howMany; i++) {
            values[i] = (int) (min + (Math.random() * (max - min)));
        }

        return values;
    }

}
